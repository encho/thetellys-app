import * as React from "react";
import styled from "styled-components";
import { Heading as BaseText } from "rebass";

const TrackInformationWrapper = styled.div`
  /* padding: 5px; */
`;

const Name = styled.div`
  /* font-family: "Righteous", cursive; */
  font-family: "Roboto", sans-serif;
  font-weight: 700;
  color: ${({ active, theme }) =>
    active ? theme.colors.palette.primary : "#555555"};
`;

const Year = styled.div`
  /* font-family: "Righteous", cursive; */
  font-family: "Roboto", sans-serif;
  font-weight: 400;
  font-size: 0.8rem;
  color: ${({ active, theme }) =>
    active ? theme.colors.palette.primary : "#555555"};
`;

const TrackInformation = ({ name, artist, album, year, artwork, active }) => (
  <TrackInformationWrapper>
    <Name active={active}>{name}</Name>
    <Year active={active}>{year}</Year>
  </TrackInformationWrapper>
);

export default TrackInformation;
