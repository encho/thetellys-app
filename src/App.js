import * as React from "react";
import { Flex, Box } from "grid-styled";
import styled, { injectGlobal, ThemeProvider } from "styled-components";
import WelcomeText from "./WelcomeText";
import Player from "./Player";

const track = {
  name: "Blut und Äther",
  artist: "Thetellys",
  album: "TBD",
  year: 2018,
  artwork:
    "https://funkadelphia.files.wordpress.com/2012/09/odesza-summers-gone-lp.jpg",
  duration: 258,
  source: "https://s3.eu-central-1.amazonaws.com/thetellys/blut_und_aether.mp3"
};

const track2 = {
  name: "Micky Maus",
  artist: "Thetellys",
  album: "TBD",
  year: 2018,
  artwork:
    "https://funkadelphia.files.wordpress.com/2012/09/odesza-summers-gone-lp.jpg",
  duration: 230,
  source: "https://s3.eu-central-1.amazonaws.com/thetellys/mickey3.mp3"
};

const track3 = {
  name: "Hurensöhne",
  artist: "Thetellys",
  album: "TBD",
  year: 2018,
  artwork:
    "https://funkadelphia.files.wordpress.com/2012/09/odesza-summers-gone-lp.jpg",
  duration: 135,
  source: "https://s3.eu-central-1.amazonaws.com/thetellys/hurensohn.mp3"
  // source: "https://s3.eu-central-1.amazonaws.com/thetellys/Hurenso%CC%88hne.m4a"
};

const track4 = {
  name: "Schlauch",
  artist: "Thetellys",
  album: "TBD",
  year: 2018,
  artwork:
    "https://funkadelphia.files.wordpress.com/2012/09/odesza-summers-gone-lp.jpg",
  duration: 172,
  source: "https://s3.eu-central-1.amazonaws.com/thetellys/Schlauch.m4a"
};

const track5 = {
  name: "Chicks on Speed",
  artist: "Thetellys",
  album: "TBD",
  year: 2017,
  artwork:
    "https://funkadelphia.files.wordpress.com/2012/09/odesza-summers-gone-lp.jpg",
  duration: 221,
  source: "https://s3.eu-central-1.amazonaws.com/thetellys/chicksfinal1.mp3"
};

const track6 = {
  name: "Flippo the Hippo",
  artist: "Thetellys",
  album: "TBD",
  year: 2013,
  artwork:
    "https://funkadelphia.files.wordpress.com/2012/09/odesza-summers-gone-lp.jpg",
  duration: 163,
  source: "https://s3.eu-central-1.amazonaws.com/thetellys/flippo-the-hippo.mp3"
};

injectGlobal`
  * { box-sizing: border-box; }
  body {
    margin: 0;
    background: #222222;
  }
`;

class MultiPlayer extends React.Component {
  state = { activeTrack: 0 };

  handlePlay = trackIndex => {
    this.setState({ activeTrack: trackIndex });
  };

  render() {
    const { activeTrack } = this.state;
    return (
      <div>
        <Flex justifyContent="center">
          <Box width={[0.9, 0.62, 1 / 2]}>
            <Player
              track={track}
              active={activeTrack === 0}
              handlePlay={() => this.handlePlay(0)}
            />
          </Box>
        </Flex>
        <Flex justifyContent="center" mt={4}>
          <Box width={[0.9, 0.62, 1 / 2]}>
            <Player
              track={track2}
              active={activeTrack === 1}
              handlePlay={() => this.handlePlay(1)}
            />
          </Box>
        </Flex>
        <Flex justifyContent="center" mt={4}>
          <Box width={[0.9, 0.62, 1 / 2]}>
            <Player
              track={track3}
              active={activeTrack === 2}
              handlePlay={() => this.handlePlay(2)}
            />
          </Box>
        </Flex>
        <Flex justifyContent="center" mt={4}>
          <Box width={[0.9, 0.62, 1 / 2]}>
            <Player
              track={track4}
              active={activeTrack === 3}
              handlePlay={() => this.handlePlay(3)}
            />
          </Box>
        </Flex>
        <Flex justifyContent="center" mt={4}>
          <Box width={[0.9, 0.62, 1 / 2]}>
            <Player
              track={track5}
              active={activeTrack === 4}
              handlePlay={() => this.handlePlay(4)}
            />
          </Box>
        </Flex>
        <Flex justifyContent="center" mt={4}>
          <Box width={[0.9, 0.62, 1 / 2]}>
            <Player
              track={track6}
              active={activeTrack === 5}
              handlePlay={() => this.handlePlay(5)}
            />
          </Box>
        </Flex>
      </div>
    );
  }
}

const TopLine = styled.div`
  height: 3px;
  background: #dc2424; /* fallback for old browsers */

  background: linear-gradient(
    to right,
    orange,
    yellow,
    green,
    cyan,
    blue,
    violet,
    #ff0085
  ); /* Standard syntax (must be last) */
`;

const App = () => (
  <ThemeProvider
    theme={{
      colors: {
        palette: { primary: "#ff0085" }
      }
    }}
  >
    <div>
      <TopLine />
      <Flex
        style={{ height: "100%" }}
        flexDirection="column"
        justifyContent="space-around"
      >
        <div>
          <WelcomeText />
          <MultiPlayer />
        </div>
      </Flex>
    </div>
  </ThemeProvider>
);

export default App;
