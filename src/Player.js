import * as React from "react";
import styled from "styled-components";
import { Flex, Box } from "grid-styled";
import PlayerContainer from "./PlayerContainer";
import TrackInformation from "./TrackInformation";
import Scrubber from "./Scrubber";
import Controls from "./Controls";

// const convertTime = timestamp => {
//   const minutes = Math.floor(timestamp / 60);
//   const seconds = timestamp - minutes * 60;
//   return minutes + ":" + (seconds < 10 ? `0${seconds}` : seconds);
// };

// const TimestampsWrapper = styled.div`
//   background-color: red;
// `;

// const Timestamps = ({ currentTime, duration }) => (
//   <TimestampsWrapper>
//     <h1>currentTime: {convertTime(currentTime)}</h1>
//     <h1>duration: {convertTime(duration)}</h1>
//   </TimestampsWrapper>
// );

const Player = ({ track, active, handlePlay }) => (
  <PlayerContainer track={track} active={active} handlePlay={handlePlay}>
    {({
      currentTime,
      currentPercent,
      playStatus,
      currentTrack,
      togglePlay,
      navigateToPercent
    }) => (
      <div>
        <Flex>
          <Box>
            <Controls
              playStatus={playStatus}
              togglePlay={togglePlay}
              active={active}
            />
          </Box>
          <Box>
            <TrackInformation
              active={active}
              name={currentTrack.name}
              artist={currentTrack.artist}
              album={currentTrack.album}
              year={currentTrack.year}
              artwork={currentTrack.artwork}
            />
          </Box>
        </Flex>
        <Flex mt={2}>
          <Box width={1}>
            <Scrubber
              active={active}
              currentPercent={currentPercent}
              navigateToPercent={navigateToPercent}
            />
          </Box>
        </Flex>
        {/* <Timestamps
          duration={currentTrack.duration}
          currentTime={currentTime}
        /> */}
      </div>
    )}
  </PlayerContainer>
);

export default Player;
