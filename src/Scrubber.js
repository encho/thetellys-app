import * as React from "react";
import styled from "styled-components";

const ScrubberWrapper = styled.div`
  background-color: #1a1a1a;
  padding: 2px;
  height: 6px;
  &.clickable:hover {
    cursor: pointer;
  }
`;

const HorizontalBar = styled.div`
  background-color: ${({ active, theme }) =>
    active ? theme.colors.palette.primary : "#555555"};
  width: ${({ percent }) => `${percent * 100}%`};
  height: 100%;
  transition: width 0.1s ease;
`;

const Scrubber = ({ currentPercent, active, navigateToPercent }) => {
  let el = null;
  const handleClick = e =>
    active && navigateToPercent(e.nativeEvent.offsetX / el.clientWidth);
  return (
    <ScrubberWrapper
      className={active ? "clickable" : null}
      onClick={handleClick}
      innerRef={x => {
        el = x;
      }}
    >
      <HorizontalBar percent={currentPercent} active={active} />
    </ScrubberWrapper>
  );
};

export default Scrubber;
