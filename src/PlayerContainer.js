import * as React from "react";

class PlayerContainer extends React.Component {
  state = { playStatus: "pause", currentTime: 0, currentPercent: 0 };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.active === false) {
      return {
        playStatus: "pause"
      };
    }
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    if (prevState.playStatus === "play" && this.state.playStatus === "pause") {
      this.pause();
    }
    if (prevState.playStatus === "pause" && this.state.playStatus === "play") {
      this.play();
    }
    return null;
  }

  play = () => {
    this.audio.play();
    let that = this;
    // TODO remove this when not needed any more
    setInterval(function() {
      const currentTime = that.audio.currentTime;
      const duration = that.props.track.duration;
      that.setState({
        currentTime: Math.floor(currentTime),
        currentPercent: currentTime / duration
      });
    }, 100);
  };

  pause = () => {
    this.audio.pause();
  };

  navigateToPercent = percent => {
    // console.log("hehehehhehe", percent);
    this.audio.currentTime = Math.floor(this.props.track.duration * percent);
    // this.audio.currentTime = this.props.track.duration;
  };

  togglePlay = () => {
    let status = this.state.playStatus;
    const audio = this.audio;
    if (status === "pause") {
      status = "play";
      this.props.handlePlay();
    } else {
      status = "pause";
    }
    this.setState({ playStatus: status });
  };

  render() {
    const { currentTime, playStatus, currentPercent } = this.state;
    const { track: currentTrack } = this.props;
    return (
      <div>
        <audio ref={ref => (this.audio = ref)}>
          <source src={currentTrack.source} />
        </audio>
        {this.props.children({
          currentPercent,
          navigateToPercent: this.navigateToPercent,
          currentTime,
          playStatus,
          currentTrack,
          togglePlay: this.togglePlay
        })}
      </div>
    );
  }
}

export default PlayerContainer;
