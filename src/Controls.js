import * as React from "react";
import styled, { withTheme } from "styled-components";

import PlayIcon from "react-icons/lib/md/play-circle-filled";
import PauseIcon from "react-icons/lib/md/pause-circle-filled";

const ControlsWrapper = styled.div`
  height: 100%;
`;

const Button = styled("button")`
  background-color: transparent;
  border: none;
  cursor: pointer;
  overflow: hidden;
  outline: none;
  height: 100%;
  width: 100%;
`;

const Controls = withTheme(({ togglePlay, playStatus, active, theme }) => {
  const color = active ? theme.colors.palette.primary : "#555555";
  return (
    <ControlsWrapper>
      <Button onClick={togglePlay}>
        {playStatus === "pause" ? (
          <PlayIcon color={color} size={28} />
        ) : (
          <PauseIcon color={color} size={28} />
        )}
      </Button>
    </ControlsWrapper>
  );
});

export default Controls;
